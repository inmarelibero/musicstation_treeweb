<?php

namespace MusicStation\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('location')
            ->add('title')
            ->add('startDate', 'datetime', array(
                'label' => 'Start date',
                'widget' => 'single_text',
                'format' => 'yyyy/MM/dd HH:mm:ss'
            ))
            ->add('endDate', 'datetime', array(
                'label' => 'Start date',
                'widget' => 'single_text',
                'format' => 'yyyy/MM/dd HH:mm:ss'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MusicStation\UserBundle\Entity\Event'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'musicstation_userbundle_event';
    }
}
