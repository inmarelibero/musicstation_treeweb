<?php

namespace MusicStation\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MusicStation\UserBundle\Entity\Event;
use MusicStation\UserBundle\Form\Type\EventType;

/**
 * Event controller.
 *
 * @Route("/user/event")
 */
class EventController extends Controller
{
    /**
     * Lists all Event entities.
     *
     * @Route("/", name="user_event")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();

        $entities = $em->getRepository('MusicStationUserBundle:Event')->findBy(
            array('artist' => $user->getArtist()),
            array('startDate' => 'ASC')
        );

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Event entity.
     *
     * @Route("/create", name="user_event_create")
     * @Method("POST")
     * @Template("MusicStationUserBundle:Event:edit.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Event();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // associate artist
            $user = $this->container->get('security.context')->getToken()->getUser();
            $entity->setArtist($user->getArtist());

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Operation successful!');

            return $this->redirect($this->generateUrl('user_event_edit', array('id' => $entity->getId())));
        }

        $this->get('session')->getFlashBag()->add('error', 'Some errors occurred!');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Event entity.
     *
     * @param Event $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Event $entity)
    {
        $form = $this->createForm(new EventType(), $entity, array(
            'action' => $this->generateUrl('user_event_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Event entity.
     *
     * @Route("/new", name="user_event_new")
     * @Method("GET")
     * @Template("MusicStationUserBundle:Event:edit.html.twig")
     */
    public function newAction()
    {
        $entity = new Event();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Event entity.
     *
     * @Route("/{id}/edit", name="user_event_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MusicStationUserBundle:Event')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $form = $this->createEditForm($entity);

        return array(
            'entity'      => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to edit a Event entity.
     *
     * @param Event $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Event $entity)
    {
        $form = $this->createForm(new EventType(), $entity, array(
            'action' => $this->generateUrl('user_event_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }

    /**
     * Edits an existing Event entity.
     *
     * @Route("/{id}/update", name="user_event_update")
     * @Method("PUT")
     * @Template("MusicStationUserBundle:Event:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MusicStationUserBundle:Event')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $form = $this->createEditForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Operation successful!');

            return $this->redirect($this->generateUrl('user_event_edit', array('id' => $id)));
        }

        $this->get('session')->getFlashBag()->add('error', 'Some errors occurred!');

        return array(
            'entity'      => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Deletes a Event entity.
     *
     * @Route("/{id}/delete", name="user_event_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MusicStationUserBundle:Event')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Event entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Operation successful!');
        }

        return $this->redirect($this->generateUrl('user_event'));
    }

    /**
     * Prints a form to delete a Event entity by id.
     *
     * @Template()
     */
    public function _deleteFormAction($id)
    {
        return array(
            'delete_form' => $this->createDeleteForm($id)->createView()
        );
    }

    /**
     * Creates a form to delete a Event entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_event_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}