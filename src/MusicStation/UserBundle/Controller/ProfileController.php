<?php

namespace MusicStation\UserBundle\Controller;

use FOS\UserBundle\Controller\ProfileController as BaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ProfileController extends BaseController
{
    /**
     * Show the user
     */
    public function showAction()
    {
        $url = $this->container->get('router')->generate('user_index');
        $response = new RedirectResponse($url);

        return $response;
    }
}
